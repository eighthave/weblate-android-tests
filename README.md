
# Weblate Android Tests

A collection of tests to help make Weblate work really smoothly with
Android _strings.xml_, supporting all of the oddities of the format.
