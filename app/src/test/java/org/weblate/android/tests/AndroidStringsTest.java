package org.weblate.android.tests;

import android.app.Instrumentation;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.test.InstrumentationRegistry;
import android.util.DisplayMetrics;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowLog;

import java.lang.reflect.Field;
import java.util.Locale;

@RunWith(RobolectricTestRunner.class)
public class AndroidStringsTest {

    private Context context;
    private AssetManager assets;
    private Configuration config;
    private Resources resources;

    @Before
    public void setUp() {
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        context = instrumentation.getTargetContext();
        assets = context.getAssets();
        config = context.getResources().getConfiguration();
        config.locale = Locale.ENGLISH;
        // Resources() requires DisplayMetrics, but they are only needed for drawables
        resources = new Resources(assets, new DisplayMetrics(), config);

        ShadowLog.stream = System.out;
    }

    @Test
    public void leadingTrailingSpace() throws NoSuchFieldException, IllegalAccessException {

        String[] names = {
                "leading_space",
                "trailing_space",
                "leading_trailing_spaces",
                "quoted_leading_space",
                "quoted_trailing_space",
                "quoted_leading_trailing_spaces",
        };
        for (String name : names) {
            Field field = R.string.class.getField(name);
            int resId = field.getInt(R.string.class);
            String string = context.getString(resId);
            System.out.println(name + " " + resId + ": -=<" + string + ">=-");
        }
    }
}